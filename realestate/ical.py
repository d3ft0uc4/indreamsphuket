import requests
from icalendar import Calendar


def get_ical(url):
    ical = requests.get(url).content
    gcal = Calendar.from_ical(ical)
    dates = []
    for component in gcal.walk():
        if component.get('DTSTART') and component.get('DTEND'):
            dates.append(component.get('DTSTART').dt.strftime("%Y-%m-%d"))
    print dates
    return dates



if __name__ == '__main__':
    get_ical('https://www.availabilitycalendar.com/ical/9549e0ff449467bcfa28d165286015e1.ics')
