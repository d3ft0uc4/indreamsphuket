# coding=utf-8
import smtplib
from email.mime.text import MIMEText
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, FormView
import realestate.settings as app_settings
from realestate.ical import get_ical
from realestate.services import *
from django import template
from datetime import datetime, date
import operator
import os
import zipfile
import StringIO
from django.http import HttpResponse


class BaseTemplateView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        return context

    def currency(self):
        return self.request.session.get('currency', 'THB')

    def currencies(self):
        return ['THB', 'RUB', 'USD', 'EUR']

    def language(self):
        return self.request.session.get('language', 'ru')

    def languages(self):
        return {'ru': 'Русский',
                'en': 'English'}

    def min_rent(self):
        return str(int(5000 * self.rates())).replace(' ', '')

    def max_rent(self):
        return str(int(120000 * self.rates())).replace(' ', '')

    def min_buy(self):
        return str(int(1000000 * self.rates())).replace(' ', '')

    def max_buy(self):
        return str(int(120000000 * self.rates())).replace(' ', '')

    def rates(self):
        r = float(get_rates(self.currency()))
        return r


@csrf_exempt
def set_currency(request):
    if request.method == 'POST':
        currency = json.loads(request.body)[0]
        request.session['currency'] = currency.replace("CUR_", "")
        request.session['currency_switched'] = True
        return HttpResponse('')


@csrf_exempt
def set_lang(request):
    if request.method == 'POST':
        l = json.loads(request.body)[0]
        request.session['language'] = l
        return HttpResponse('')


class IndexView(BaseTemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['featured_listings'] = Listing().coll.find(
            {"Status.displayValue": "Активный", "LeadType.displayValue": "Продажа"}, limit=3,
            sort=[('ListingPriority', 1)])
        context['featured_listings_rent'] = Listing().coll.find(
            {"Status.displayValue": "Активный", "LeadType.displayValue": {'$ne': "Продажа"}}, limit=3,
            sort=[('ListingPriority', 1)])
        context['revslider_listings'] = Listing().coll.find(
            {"Status.displayValue": "Активный", "IsSlider": True}, limit=3)
        context['districts'] = list(District().coll.find())
        context['property_types'] = PropertyType().coll.find()
        return context

    template_name = 'index2.html'


class AboutView(BaseTemplateView, FormView):
    template_name = 'about/index.html'


class PropertiesView(BaseTemplateView, FormView):
    template_name = 'properties/index.html'


class RentView(BaseTemplateView, FormView):
    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['action'] = 'Аренда'
        context['status'] = 'for-rent'
        return context

    template_name = 'properties/index.html'


class BuyView(BaseTemplateView, FormView):
    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['action'] = 'Продажа'
        context['status'] = 'for-buy'
        return context

    template_name = 'properties/index.html'


@csrf_exempt
def search(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        json_data['lower'] = int(float(filter(lambda x: x in '0123456789.', json_data['lower'])))
        json_data['upper'] = int(float(filter(lambda x: x in '0123456789.', json_data['upper'])))
        request.session['search'] = json_data
        return HttpResponse('')


@csrf_exempt
def handle_forms(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        msg_str = u'Тема: {0}\nИмя: {1}\nEmail:{2}\nТелефон:{3}\nКомментарий:{4}' \
            .format(
            json_data['tag'],
            json_data['name'],
            json_data['email'],
            json_data['phone'],
            json_data['message'],
        )
        request.session['form_submitted'] = True
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        msg = MIMEText(msg_str.encode('utf-8'), _charset='utf-8')
        server.ehlo()
        server.login(app_settings.GMAIL_USER, app_settings.GMAIL_PASSWORD)
        server.sendmail(app_settings.GMAIL_USER, app_settings.MAIL_RECEIVER, msg.as_string())
        server.quit()
        return HttpResponse()


def date_to_nth_day(date):
    new_year_day = datetime(year=date.year, month=1, day=1)
    return (date - new_year_day).days + 1


def get_price_from_price_matrix(cur_day, price_matrix, total_days, prefix):
    for item in price_matrix:
        if cur_day - item['nDayStart' + prefix] >= 0 >= cur_day - item['nDayFinish' + prefix]:
            if total_days >= 30:
                return item['price_month'] / 30.0
            elif total_days >= 14:
                return item['price_two_weeks'] / 14.0
            elif total_days >= 7:
                return item['price_week'] / 7.0
            else:
                return item['price_day']


@csrf_exempt
def calc(request):
    date_start = datetime.strptime(request.POST.get('date_start'), "%Y-%m-%d") + timedelta(days=1)
    date_end = datetime.strptime(request.POST.get('date_end'), "%Y-%m-%d") + timedelta(days=1)
    prop_id = request.POST.get('property')
    rates = get_rates(request.session.get('currency', 'THB'))
    count = (date_end - date_start).days
    price_matrix = list(reversed(get_price_matrix(prop_id, rates)))
    default_price = get_property_price(prop_id) * rates
    if count >= 30:
        default_price *= 0.7
    elif count >= 14:
        default_price *= 0.8
    elif count >= 7:
        default_price *= 0.9
    price = 0
    while (date_end - date_start).days > 0:
        cur_day_booked = date_to_nth_day(date_start)
        prefix = ''
        if date_start.year % 4 == 0:
            prefix = '_leap'
        day_price = get_price_from_price_matrix(cur_day_booked, price_matrix, count, prefix)
        if day_price:
            price += day_price
        else:
            price += default_price
        date_start += timedelta(days=1)
    label = 'ночей'
    if count == 1:
        label = 'ночь'
    elif 2 <= count <= 4:
        label = 'ночи'
    return HttpResponse(json.dumps(
        {'price': int(price), 'count': count, 'label': label}
    ))


# TODO calculactions
def get_property_price(prop_id):
    for l in Listing().coll.find({"Id": prop_id}):
        return l['BCPricePerDay']


@csrf_exempt
def handle_ajax(request):
    action = request.POST.get('action')
    if action == 'houzez_header_map_listings':
        return HttpResponse(json.dumps({}))
    elif action == 'houzez_loadmore_properties':
        prop_limit = int(request.POST.get('prop_limit'))
        paged = int(request.POST.get('paged'))
        type = request.POST.get('type')
        if type == 'rent':
            context = {'featured_listings_rent': Listing().coll.find(
                {"Status.displayValue": "Активный", "LeadType.displayValue": {'$ne': "Продажа"}}, limit=prop_limit,
                skip=prop_limit * (paged - 1), sort=[('ListingPriority', 1)]),
                'view': {
                    'currency': request.session.get('currency', 'THB'),
                    'rates': get_rates(request.session.get('currency', 'THB')),
                    'language': request.session.get('language', 'ru')
                }
            }
            return render_to_response('includes/index_featured_inner_rent.html', context)
        else:
            context = {'featured_listings': Listing().coll.find(
                {"Status.displayValue": "Активный", "LeadType.displayValue": "Продажа"}, limit=prop_limit,
                skip=prop_limit * (paged - 1), sort=[('ListingPriority', 1)]),
                'view': {
                    'currency': request.session.get('currency', 'THB'),
                    'rates': get_rates(request.session.get('currency', 'THB')),
                    'language': request.session.get('language', 'ru')
                }
            }
            return render_to_response('includes/index_featured_inner.html', context)
    elif action == 'change_sort':
        request.session['order_by'] = request.POST.get('order_by')
        return HttpResponse(json.dumps({"success": True, "notification": False}))
    elif action == 'houzez_create_print':
        id = request.POST.get('propid')
        context = {}
        l = Listing().coll.find_one(
            {"Status.displayValue": "Активный", "Id": id})
        context['listing'] = l
        currency = request.session.get('currency', 'THB')
        context['rates'] = float(get_rates(currency))
        context['currency'] = currency
        return render_to_response('print_form.html', context)
    else:
        return HttpResponse(json.dumps({"success": True, "notification": False}))


def handle404(request):
    response = render_to_response(
        '404.html',
        context_instance=RequestContext(request)
    )
    response.status_code = 404

    return response


class ContactView(BaseTemplateView):
    template_name = 'contact/index.html'


class SinglePropertyView(BaseTemplateView):
    template_name = 'single_property.html'

    def get_context_data(self, **kwargs):
        slug = self.kwargs['slug']
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        if slug:
            l = Listing().coll.find_one(
                {"Status.displayValue": "Активный", "slug": slug})
            l['VideoLink'] = l['VideoLink'].replace("watch?v=", "embed/")
            context['listing'] = l
            if l['LeadType']['value'] == '08ca044e-5cf5-4c5f-a091-f6aacd5b53f6':
                context['is_rent'] = False
            else:
                context['is_rent'] = True

            context['rates'] = float(get_rates(self.currency()))
            context['currency'] = self.currency()
            gt = 1.0
            lt = 1.0
            delta = 0.1
            limit = 5
            q = {"Id": {'$ne': l['Id']},
                 "Status.displayValue": l['Status']['displayValue'],
                 "LeadType.displayValue": l['LeadType']['displayValue'],
                 "BCSTRINGBedroom": l["BCSTRINGBedroom"],
                 "PropertyType.displayValue": l['PropertyType']['displayValue'],
                 'Price': {"$gt": 0 * l['Price'], "$lt": 10 * l['Price']}
                 }
            similar_properties = Listing().coll.find(q, limit=limit)
            while similar_properties.count() < limit and gt > 0.0:
                gt -= delta
                lt += delta
                similar_properties = Listing().coll.find(q, limit=limit)
            context['similar_properties'] = similar_properties
            lcz = {}
            for k in l:
                v = l[k]
                if not v:
                    continue

                if type(v) is dict:
                    v = v['displayValue']
                lcz_k = localizable_strings.get(k)
                if lcz_k in ['Описание', 'ID Сайта', 'Валюта', 'Создал', 'Ссылка на галерею', 'Тип объекта',
                             'Тип листинга', 'ID объекта', 'Ссылка', 'Изменил', 'Дата начала', 'Статус',
                             'Дата создания', 'Id', 'Контакт с сайта', 'Дата изменения', 'Широта (служебная)',
                             'Стоимость', 'Название', 'Страна', 'Широта', 'Долгота', 'Район города',
                             'Ссылка на Яндекс карты', 'Фото', 'Ответственный', 'Долгота (служебная)']:
                    continue
                if lcz_k and v:
                    lcz[lcz_k] = v
            context['listing_lcz'] = lcz
            mp = {}
            bd = {}
            bt = {}
            tech = {}
            inf_3km = {}
            opl_otd = {}
            dop_op = {}
            v_st_vkl = {}
            usl_kons = {}
            for k in lcz:
                if k in main_props:
                    mp[k] = (lcz[k], mp_dict[k])
                elif k in blag_doma:
                    bd[k] = lcz[k]
                elif k in blag_territorii:
                    bt[k] = lcz[k]
                elif k in technika:
                    tech[k] = lcz[k]
                elif k in infrastructure_3km:
                    inf_3km[k] = lcz[k]
                elif k in oplata_otdelno:
                    opl_otd[k] = lcz[k]
                elif k in dop_oplata:
                    dop_op[k.replace('(ДО)', '')] = lcz[k]
                elif k in v_stoimost_vkl:
                    v_st_vkl[k.replace('(вкл)', '')] = lcz[k]
                elif k in uslugi_konsierzha:
                    usl_kons[k] = lcz[k]
            props = [
                ('Благоустройство дома', bd),
                ('Благоустройство территории', bt),
                ('Техника', tech),
                ('Инфраструктура в радиусе 3км', inf_3km),
                ('Оплачивается отдельно', opl_otd),
                ('Дополнительно оплачивается', dop_op),
                ('В стоимость включено', v_st_vkl),
                ('Услуги консьержа', usl_kons),
            ]
            props = filter(lambda (x, y): len(y) > 0, props)
            groups = zip(*[props[i::2] for i in range(2)])
            context['groups'] = groups
            context['mp'] = mp
            group_name = l['BCListingGroup']
            if group_name:
                group_name = group_name['displayValue']
                same_group = list(Listing().coll.find({'BCListingGroup.displayValue': group_name}))
                if len(same_group) > 0:
                    context['object_group'] = group_name
                    context['object_group_list'] = same_group
            context['dates'] = get_disabled_dates(l['Id'])
            context['price_matrix'] = get_price_matrix(l['Id'], float(get_rates(self.currency())))
        return context


def get_disabled_dates(listing_id):
    result = set()
    for period in BCRentPeriods().coll.find({'BCListing': listing_id}):
        start_date = date(
            day=int(period['BCStartDay']),
            month=int(period['BCStartMonth']),
            year=int(period['BCStartYear'])) - timedelta(days=1)
        finish_date = date(
            day=int(period['BCFinishDay']),
            month=int(period['BCFinishMonth']),
            year=int(period['BCFinishYear'])) - timedelta(days=1)
        while start_date < finish_date:
            result.add(start_date.strftime("%Y-%m-%d"))
            start_date = start_date + timedelta(days=1)
    print list(result)
    return list(result)


def get_price_matrix(listing_id, rates):
    result = list()
    for item in BCPriceMatrix().coll.find({'BCListing': listing_id}):
        price_day = item['BCPriceDay']
        price_week = item['BCPriceWeek']
        price_two_weeks = item['BCPriceTwoWeeks']
        price_month = item['BCPriceMonth']
        if not price_week:
            price_week = price_day * 7 * 0.9
        if not price_two_weeks:
            price_two_weeks = price_day * 14 * 0.8
        if not price_month:
            price_month = price_day * 30 * 0.7
        d = {
            'start': item['BCStartDay'] + '.' + item['BCStartMonth'],
            'finish': item['BCFinishDay'] + '.' + item['BCFinishMonth'],
            'threshold': item['BCPeriodThreshold'],
            'price_day': price_day * rates,
            'price_week': price_week * rates,
            'price_two_weeks': price_two_weeks * rates,
            'price_month': price_month * rates,
            'nDayStart': item['nDayStart'],
            'nDayFinish': item['nDayFinish'],
            'nDayStart_leap': item['nDayStart_leap'],
            'nDayFinish_leap': item['nDayFinish_leap']
        }
        result.append(d)
    result.sort(key=operator.itemgetter('threshold'))
    return result


class BlogView(BaseTemplateView):
    template_name = 'blog/index.html'


class ServiceView(BaseTemplateView):
    template_name = 'service.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['services'] = KnowledgeBase().coll.find({"Type.displayValue": "Сайт: Наши услуги для гостей"})
        return context


class NewsView(BaseTemplateView):
    template_name = 'news.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['articles'] = KnowledgeBase().coll.find({"Type.displayValue": "Сайт: Наши услуги для гостей"})
        context['title'] = 'Новости'
        return context


class SingleNewsView(BaseTemplateView):
    template_name = 'single_news.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        slug = self.kwargs['slug']
        context['article'] = KnowledgeBase().coll.find(
            {"Type.displayValue": "Сайт: Наши услуги для гостей", "slug": slug})[0]
        return context


class StatiView(BaseTemplateView):
    template_name = 'news.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['articles'] = KnowledgeBase().coll.find({"Type.displayValue": "Сайт: Наши услуги для гостей"})
        context['title'] = 'Статьи'
        return context


class SingleStatiView(BaseTemplateView):
    template_name = 'single_news.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        slug = self.kwargs['slug']
        context['article'] = KnowledgeBase().coll.find(
            {"Type.displayValue": "Сайт: Наши услуги для гостей", "slug": slug})[0]
        return context


class ServiceView2(BaseTemplateView):
    template_name = 'service.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['services'] = KnowledgeBase().coll.find({"Type.displayValue": "Сайт: Наши услуги для гостей"})
        return context


class AdvancedSearchView(BaseTemplateView):
    template_name = 'advanced_search.html'

    def get_context_data(self, **kwargs):
        per_page = 24
        page = self.request.GET.get("paged")
        if not page:
            page = 1
        page = int(page)
        order_by = self.request.session.get('order_by')
        sort = [("$natural", 1)]
        if order_by == 'date_asc':
            sort = [("ModifiedOn", 1)]
        elif order_by == 'date_desc':
            sort = [("ModifiedOn", -1)]
        elif order_by == 'price_asc':
            sort = [("Price", 1)]
        elif order_by == 'price_desc':
            sort = [("Price", -1)]

        q, d = self.get_query(self.request.GET)
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['date_start'] = self.request.GET.get("date_start")
        context['page'] = page
        context['order_by'] = order_by
        context['listings'] = list(Listing().coll.find(q).sort(sort).skip((page - 1) * per_page).limit(per_page))
        context['count'] = Listing().coll.find(q).count()
        if page == 1:
            context['prev'] = False
        else:
            context['prev'] = True
            context['prev_page'] = page - 1
        if len(list(Listing().coll.find(q).sort(sort).skip(page * per_page).limit(per_page))):
            context['next'] = True
            context['next_page'] = page + 1
        else:
            context['next'] = False
        context['districts'] = District().coll.find()
        context['property_types'] = PropertyType().coll.find()
        if self.request.session.get('currency_switched'):
            context['currency_switched'] = True
        self.request.session['currency_switched'] = False
        for k, v in d.items():
            context[k] = v
        return context

    def get_query(self, args):
        q = {"Status.displayValue": "Активный"}
        d = {}
        if args.get('id'):
            q['BCSTRINGObjectID'] = args.get('id')
            return q, d
        status = None
        if args.get('status'):
            status = args['status']
            d['status'] = args['status']
            if status == 'for-rent':
                q['LeadType.displayValue'] = {'$ne': 'Продажа'}
            else:
                q['LeadType.displayValue'] = 'Продажа'
        if args.get('min-price') and args.get('max-price'):
            min_price = int(int(filter(lambda x: x.isdigit(), args['min-price'])) / self.rates())
            max_price = int(int(filter(lambda x: x.isdigit(), args['max-price'])) / self.rates())
            d['min_price_int'] = min_price
            d['max_price_int'] = max_price
            d['min_price'] = args.get('min_price')
            d['max_price'] = args.get('max_price')
            if status == 'for-rent':
                q['BCPricePerDay'] = {'$gte': min_price, '$lte': max_price}
            else:
                q['Price'] = {'$gte': min_price, '$lte': max_price}
        if args.get('type'):
            d['type'] = args.getlist('type')
            q['PropertyType.value'] = {'$in': args.getlist('type')}
        if args.get('location'):
            d['location'] = args.getlist('location')
            q['District.value'] = {'$in': args.getlist('location')}
        if args.get('bedrooms'):
            d['bedrooms'] = map(int, args.getlist('bedrooms'))
            q['BCSTRINGBedroom'] = {'$in': args.getlist('bedrooms')}
        print(q)
        return q, d


class VillaHotelView(BaseTemplateView):
    template_name = 'villa-ili-otel.html'


class LagunaView(BaseTemplateView):
    template_name = 'laguna-outrigger-village-na-phukete.html'


class AddNewPropertyView(BaseTemplateView):
    template_name = 'add-new-property.html'


class InvestoramView(BaseTemplateView):
    template_name = 'investoram.html'


class ExcursionsView(BaseTemplateView):
    template_name = 'excursions.html'


def single_excursion(request, slug):
    try:
        return render_to_response('excursions/{0}.html'.format(slug))
    except:
        return render_to_response('404.html')


class PhotosView(BaseTemplateView):
    template_name = 'fotogalereya.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['urls'] = [
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-1e3aec3972de4e9936fa723013a5c7a41ddc7bb3.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20384263_1708247835886834_3441934286252683215_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-theclassycloud-phuket-bikini-lookbook-anita-1-von-10.jpg',
            'http://www.indreamsphuket.ru/uploads/page/Temples-of-Phuket.jpg',
            'http://www.indreamsphuket.ru/uploads/page/theclassycloud-hotel-review-movenpick-bangtao-phuket-8-von-8.jpg',
            'http://www.indreamsphuket.ru/uploads/page/TBV02.jpg',
            'http://www.indreamsphuket.ru/uploads/page/photo-1421284621639-884f4129b61d.jpeg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-naithon-beach-phuket-thailand.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-LP-aerial-Property-version.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-maxresdefault-2.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-Liam-Collard-Photography_Paresa-Phuket_Indian-Wedding-Photography_Thailand-Wedding-Photographer-1021-1.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-Kamala_Beach1.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-lady-kathryn-3-1600x892.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-Just-Another-Sunset-in-Phuket-Patong-Beach-Thailand-Rabii-Kessis-%C2%A9.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-IMG_6266_2.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-cuppajyo_travelblogger_fashionblogger_twinpalmsphuket_thailand_phuket_bestresorts_9.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-beach.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-a3a551dad76fe00a611ca942baf29c3b.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-19761903_116693852278891_8903609406680203264_n.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20562645_10214824294587573_1066239999_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-18444500_295088294276611_3359703884867043328_n.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20038853_1475225882522365_4274511407157279002_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-131_1683182093.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20016004_1475226155855671_6944177000317225865_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-21765386_1685364968175121_6797458094441575494_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20272737_1672361019475516_1055321179337868278_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-2.-Maikhao-Asia-speedboat1.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-21129024_1680491301995821_3418566588210720340_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20199461_1669431469768471_829015174800922216_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-21122332_1666410300070588_9040423890320951831_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20934908_1659542304090721_3496957560991825853_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20643141_1648212341890384_7829829292386124725_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20689997_1648920605152891_2908831543583549759_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20507864_1642297052481913_3817209085825517102_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20538205_1657614334283518_8043784156580187730_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-25286799_1633127273398891_6130395852102157940_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-25956156_1617570071621278_6630367233707341094_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-18216447_1537597506285202_2684434087418336467_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-20529547_1624410734270545_8829039049975687960_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-17622040_1502492016462418_4738967555985612855_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-16587310_1452895094755444_680531907894983841_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-16403311_1439243366120617_5541101680380465781_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-16143608_1431474436897510_7328808742777976542_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-15288591_1379416228769998_1278109983090606216_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-15128806_1359794774065477_5053665596477689952_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-15056420_1356888784356076_4778426090556963912_n.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-15000786_1349456455099309_30404591144112018_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-14556688_1309725282405760_8363084885324506724_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-14556596_1313561365355485_2825255660176169053_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-13708189_1247313225313633_964301889919181779_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-13708189_1247313225313633_964301889919181779_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-13522705_1228867857158170_2106598706963809084_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-14524444_1308059735905648_7749354800257852959_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-13474957_1228867900491499_7101166018669722580_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-13217092_1205679566143666_6699526907458761_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-13415569_1221193311258958_8895733498535987439_o.jpg',
            'http://www.indreamsphuket.ru/uploads/page/thumbnails/416x277-1.jpg'
        ]
        return context


@csrf_exempt
def getfiles(request):
    id = request.GET.get("id")
    if not id:
        return HttpResponse()

    listing = Listing().coll.find({"_id": id})
    if not listing:
        return HttpResponse()
    urls = map(lambda x: x['link'], listing[0]['photos'])
    zip_subdir = "Photos"
    zip_filename = "%s.zip" % zip_subdir

    s = StringIO.StringIO()

    zf = zipfile.ZipFile(s, "w")

    for url in urls:
        path = '/tmp/' + url.split('/')[-1].split('?')[0]
        download_file(url, path)
        fdir, fname = os.path.split(path)
        zip_path = os.path.join(zip_subdir, fname)

        zf.write(path, zip_path)

    zf.close()

    resp = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
    resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

    return resp


def download_file(url, path):
    r = requests.get(url, stream=True)
    if r.status_code == 200:
        with open(path, 'wb') as f:
            for chunk in r:
                f.write(chunk)
