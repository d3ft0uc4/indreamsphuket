from django.db import models
from django.utils.translation import ugettext_lazy as _
import time


#TODO Remove this
class RecommendedListing(models.Model):
    bpm_id = models.CharField(_('bpm_id'), max_length=50)
    name = models.CharField(_('name'), max_length=250)
    address = models.CharField(_('address'), max_length=250)
    price = models.CharField(_('price'), max_length=250)
    timestamp = models.IntegerField(_('timestamp'))

    def as_bpm(self):
        return {
            'Id': self.bpm_id,
            'Name': self.name,
            'Address': self.address,
            'Price': self.price
        }
