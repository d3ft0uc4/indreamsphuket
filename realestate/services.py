# coding=utf-8
from django.http import HttpResponse
from realestate.bpm.constants import *
from realestate.bpm.helper_classes import *
from realestate.exceptions import SecurityException
from realestate.listing.models import *
from . import settings as app_settings
from realestate.bpm import constants
import requests
import json
from dateutil import parser
from bs4 import BeautifulSoup
from datetime import *
from time import time as tm
import icalendar
from dateutil.rrule import *

headers = {'Content-Type': 'application/json'}


def date_to_nth_day(date):
    new_year_day = datetime(year=date.year, month=1, day=1)
    return (date - new_year_day).days + 1


def auth():
    base_url = app_settings.BPM_BASE_URL
    username = app_settings.BPM_USERNAME
    password = app_settings.BPM_PASSWORD
    credentials = {
        'UserName': username,
        'UserPassword': password
    }
    auth_url = get_auth_url(base_url)

    session = requests.session()
    auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
    csrf_token = session.cookies.get_dict().get('BPMCSRF', None)
    if auth.status_code == 200:
        return AuthHolder(session, csrf_token)
    else:
        raise SecurityException(auth.text)


def get_rates(cur_to='THB'):
    if cur_to == 'THB':
        return 1.0
    else:
        return float(Rates().coll.find_one({'_id': cur_to})['rates'])


def update_rates():
    try:
        for c in ['RUB', 'USD', 'EUR']:
            r = json.loads(
                requests.get(
                    'http://free.currencyconverterapi.com/api/v5/convert?q=THB_' + c + '&compact=y').text)['THB_' + c][
                'val']
            Rates().coll.save({'_id': c, 'rates': r})
    except:
        pass


def get_sys_image(guid):
    auth_holder = auth()
    url = get_sys_image_url(app_settings.BPM_BASE_URL, guid)
    h = headers
    h['BPMCSRF'] = auth_holder.token
    response = auth_holder.session.get(url, headers=headers, allow_redirects=False)
    return response.content


def get_listing_image(guid):
    auth_holder = auth()
    url = get_listing_image_url(app_settings.BPM_BASE_URL, guid)
    h = headers
    h['BPMCSRF'] = auth_holder.token
    response = auth_holder.session.get(url, headers=headers, allow_redirects=False)
    return response.content


def send_to_bpm(query):
    auth_holder = auth()
    url = get_query_endpoint(app_settings.BPM_BASE_URL, 'Select')
    h = headers
    h['BPMCSRF'] = auth_holder.token
    response = auth_holder.session.post(url, data=query, headers=headers, allow_redirects=False).json()
    if app_settings.DEBUG:
        print response
    return response['rows']


def get_listing_count():
    query = count_entities('Listing')
    return send_to_bpm(query)[0]['IdCount']


def get_listings(row_count=10, columns=['Id', 'Address', 'Name', 'Price']):
    query = select_entities('Listing', row_count=row_count, columns=columns)
    return send_to_bpm(query)


def get_listing_photo(listing_id, position=0):
    query = select_by_column('ListingGalleryImage', filters={'Listing': listing_id, 'Position': position},
                             columns=['Id', 'Link'])
    return send_to_bpm(query)


def get_agents():
    query = select_entities('Employee', columns=['Id', 'Name', 'Contact', 'Job'])
    employees = send_to_bpm(query)
    for e in employees:
        query = select_by_column('ContactCommunication', filters={'Contact': e['Contact']['value']}, columns=['Number'])
        communications = send_to_bpm(query)
        query = select_by_column('Contact', filters={'Id': e['Contact']['value']}, columns=['Photo'])
        photo = send_to_bpm(query)[0]['Photo']
        if photo:
            ph_id = photo['value']
            p = get_sys_image(ph_id)
            e['Photo'] = p.encode("base64")
        e['Communications'] = communications
        if e['Job']:
            e['Job'] = e['Job']['displayValue']
    return employees


def human_readable_date(date_str):
    dt = parser.parse(date_str) + datetmdelta(hours=3)
    months = {'Jan': 'января',
              'Feb': 'февраля',
              'Mar': 'марта',
              'Apr': 'апреля',
              'May': 'мая',
              'Jun': 'июня',
              'Jul': 'июля',
              'Aug': 'августа',
              'Sep': 'сентября',
              'Oct': 'октября',
              'Nov': 'ноября',
              'Dec': 'декабря'}
    dt = dt.strftime("%d %b в %H:%M")
    for k, v in months.items():
        dt = dt.replace(k, str(v))
    return dt


def sync_listings():
    esn = 'Listing'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    listings = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=[],
                                row_count=20000, page_num=page_num)
        listings.extend(send_to_bpm(query))
    p = []
    for l in listings:
        l['_id'] = l['Id']
        if not l['ListingPriority']:
            l['ListingPriority'] = 9223372036854775800
        l['slug'] = Listing()._generate_valid_slug(l['Name'], l['Id'])
        l['photos_new'] = []
        p.append(l['Id'])
        Listing().coll.save(l)
    # Listing().coll.remove({"_id": {'$nin': p}})
    print "Sync listing complete in {} sec".format(int(tm() - t1))


def sync_districts():
    esn = 'District'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    districts = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=[],
                                row_count=20000, page_num=page_num)
        districts.extend(send_to_bpm(query))
    p = []
    for d in districts:
        d['_id'] = d['Id']
        if Listing().coll.find({'Status.displayValue': 'Активный', 'District.value': d['Id']}).count() > 0:
            p.append(d['Id'])
            District().coll.save(d)
    District().coll.remove({"_id": {'$nin': p}})
    print "Sync districts in {} sec".format(int(tm() - t1))


def sync_property_types():
    esn = 'PropertyType'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    districts = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=[],
                                row_count=20000, page_num=page_num)
        districts.extend(send_to_bpm(query))
    p = []
    for d in districts:
        if Listing().coll.find({'Status.displayValue': 'Активный', 'PropertyType.value': d['Id']}).count() > 0:
            p.append(d['Id'])
            d['_id'] = d['Id']
            PropertyType().coll.save(d)
    PropertyType().coll.remove({"_id": {'$nin': p}})
    PropertyType().coll.remove({"_id": "911f7475-81e9-4b8a-b322-5b6ba30e03c4"})
    print "Sync property types in {} sec".format(int(tm() - t1))


def sync_listing_images():
    esn = 'ListingGalleryImage'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    districts = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=['Id', 'Name', 'Listing', 'Size', 'Position', 'Link'],
                                row_count=20000, page_num=page_num)
        districts.extend(send_to_bpm(query))
    p = []
    for d in districts:
        if d['Listing']:
            if d['Link']:
                d['_id'] = d['Id']
                p.append(d['_id'])
                ListingGalleryImage().coll.save(d)
                Listing().coll.update({"_id": d['Listing']['value']},
                                      {"$push": {'photos_new': {'$each': [{'pos': d['Position'], 'link': d['Link']}]}}})

    for l in Listing().coll.find():
        photos = l.get('photos_new', [])
        l['photos'] = photos
        l['photos_new'] = []
        l['main_photo'] = ''
        for p in photos:
            if p['pos'] == 0:
                l['main_photo'] = p['link']
        Listing().coll.save(l)
    print "Sync images in {} sec".format(int(tm() - t1))


def sync_listing_images_content():
    t1 = tm()
    for d in ListingGalleryImage().coll.find():
        path = os.path.join(app_settings.CONTENT_PATH, d['Name'])
        if not os.path.isfile(path):
            content = get_listing_image(d['Id'])
            with open(path, "wb") as f:
                print 'Saving content to {}'.format(path)
                f.write(content)
    print "Sync images in {} sec".format(int(tm() - t1))


def sync_knowledge_base():
    esn = 'KnowledgeBase'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    districts = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=[],
                                row_count=20000, page_num=page_num)
        districts.extend(send_to_bpm(query))
    p = []
    for d in districts:
        p.append(d['Id'])
        d['_id'] = d['Id']
        d['slug'] = slugify(unidecode(d['Name']))
        imgs = []
        preview = ''
        html = ''
        if d['Notes']:
            html = BeautifulSoup(d['Notes'])
            for i in html.find_all('img'):
                imgs.append(str(i))
                i.extract()
            if len(html.find_all('p')):
                preview = str(html.find_all('p')[0])
                # print html
        d['images'] = imgs
        if len(imgs):
            i = BeautifulSoup(imgs[0])
            i['height'] = '162px'
            d['main_image'] = str(i)
        d['preview'] = preview
        # d['Notes'] = str(html)
        KnowledgeBase().coll.save(d)
    KnowledgeBase().coll.remove({"_id": {'$nin': p}})
    print "Sync knowledge base in {} sec".format(int(tm() - t1))


def sync_rent_periods():
    esn = 'BCRentPeriods'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    objs = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=[],
                                row_count=20000, page_num=page_num)
        objs.extend(send_to_bpm(query))
    p = []
    for d in objs:
        p.append(d['Id'])
        d['_id'] = d['Id']
        d['BCListing'] = d['BCListing']['value']
        d['BCStartDay'] = d['BCStartDay']['displayValue']
        d['BCStartMonth'] = d['BCStartMonth']['displayValue']
        d['BCStartYear'] = d['BCStartYear']['displayValue']
        d['BCFinishDay'] = d['BCFinishDay']['displayValue']
        d['BCFinishMonth'] = d['BCFinishMonth']['displayValue']
        d['BCFinishYear'] = d['BCFinishYear']['displayValue']

        BCRentPeriods().coll.save(d)
    BCRentPeriods().coll.remove({"_id": {'$nin': p}})
    print "Sync rent periods in {} sec".format(int(tm() - t1))


def sync_price_matrix():
    esn = 'BCPriceMatrix'
    t1 = tm()
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    objs = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=[],
                                row_count=20000, page_num=page_num)
        objs.extend(send_to_bpm(query))
    p = []
    for d in objs:
        p.append(d['Id'])
        d['_id'] = d['Id']
        d['BCListing'] = d['BCListing']['value']
        d['BCStartDay'] = d['BCStartDay']['displayValue']
        d['BCStartMonth'] = d['BCStartMonth']['displayValue']
        d['BCFinishDay'] = d['BCFinishDay']['displayValue']
        d['BCFinishMonth'] = d['BCFinishMonth']['displayValue']
        d['BCPeriodThreshold'] = d['BCPeriodThreshold']
        d['BCPriceDay'] = d['BCPriceDay']
        date_start = datetime.strptime('2018' + '-' + d['BCStartMonth'] + '-' + d['BCStartDay'], "%Y-%m-%d")
        date_finish = datetime.strptime('2018' + '-' + d['BCFinishMonth'] + '-' + d['BCFinishDay'], "%Y-%m-%d")
        d['nDayStart'] = date_to_nth_day(date_start)
        d['nDayFinish'] = date_to_nth_day(date_finish)
        date_start = datetime.strptime('2020' + '-' + d['BCStartMonth'] + '-' + d['BCStartDay'], "%Y-%m-%d")
        date_finish = datetime.strptime('2020' + '-' + d['BCFinishMonth'] + '-' + d['BCFinishDay'], "%Y-%m-%d")
        d['nDayStart_leap'] = date_to_nth_day(date_start)
        d['nDayFinish_leap'] = date_to_nth_day(date_finish)
        BCPriceMatrix().coll.save(d)
    BCPriceMatrix().coll.remove({"_id": {'$nin': p}})
    print "Sync rent periods in {} sec".format(int(tm() - t1))


def sync(request):
    try:
        update_rates()
        sync_listings()
        sync_districts()
        sync_property_types()
        sync_listing_images()
        sync_knowledge_base()
        sync_rent_periods()
        sync_price_matrix()
        return HttpResponse('ok')
    except Exception as e:
        return HttpResponse(e.message)
