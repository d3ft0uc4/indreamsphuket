import json

from constants import *


class Serializable:
    def __init__(self, **kwargs):
        self.__dict__ = kwargs

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4, encoding='windows-1251')

    def __setitem__(self, key, value):
        setattr(self, key, value)


class Parameter(Serializable):
    def __init__(self, value):
        Serializable.__init__(self)
        self.expressionType = expression_types['Parameter']
        self.parameter = Serializable()
        self.parameter.value = value


class Guid(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Guid']


class Text(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Text']


class Integer(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, int(value))
        self.parameter.dataValueType = data_value_types['Integer']


class Float(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, float(value))
        self.parameter.dataValueType = data_value_types['Float']


class Money(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Money']


class DateTime(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['DateTime']


class Date(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Date']
        self.parameter.value = value


class Time(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Time']


class Lookup(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Lookup']


class Enum(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Enum']


class Boolean(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, bool(value))
        self.parameter.dataValueType = data_value_types['Boolean']


class Blob(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Blob']


class Image(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Image']


class ImageLookup(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Blob']


class Color(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Color']


class Mapping(Parameter):
    def __init__(self, value):
        Parameter.__init__(self, value)
        self.parameter.dataValueType = data_value_types['Mapping']


class BasicObject(Serializable):
    def __init__(self, operation_type=OperationTypes.INSERT.value):
        Serializable.__init__(self)
        self.rootSchemaName = None
        self.columnValues = Serializable()
        self.columnValues.items = Serializable()
        self.operationType = operation_type

    def __getattr__(self, item):
        if item == 'items':
            return self.columnValues.__dict__[item]
        elif item not in (['rootSchemaName', 'columnValues', 'operationType', 'filters', 'columns']):
            return self.columnValues.items.__dict__[item].parameter.value
        else:
            return self.__dict__[item]

    def __setattr__(self, key, value):
        if key == 'items':
            self.columnValues[key] = value
        elif key not in (['rootSchemaName', 'columnValues', 'operationType', 'filters', 'columns']):
            self.columnValues.items.__dict__[key] = value
        else:
            self.__dict__[key] = value


class TSObject(BasicObject):
    def __init__(self, root_schema_name, operation_type=OperationTypes.SELECT.value):
        BasicObject.__init__(self, operation_type)
        self.rootSchemaName = root_schema_name


class IdFilter(Serializable):
    def __init__(self, _id):
        Serializable.__init__(self)
        self.items = Serializable(primaryColumnFilter=
        Serializable(
            filterType=1,
            comparisonType=3,
            isEnabled=True,
            trimDateTimeParameterToDate=False,
            leftExpression=Serializable(
                expressionType=1,
                functionType=1,
                macrosType=34
            ),
            rightExpression=Serializable(
                expressionType=2,
                parameter=Serializable(
                    dataValueType=0,
                    value=_id
                )
            )
        ))
        self.logicalOperation = 0
        self.isEnabled = True
        self.filterType = 6


class OrderDirection(Enum):
    NONE = 0
    ASC = 1
    DESC = 2


class SelectQueryExpressionType(Enum):
    SchemaColumn = 0
    Function = 1
    Parameter = 2
    SubQuery = 3
    ArithmeticOperation = 4


class AggregationType(Enum):
    AVG = 0
    COUNT = 1
    MAX = 2
    MIN = 3
    NONE = 4
    SUM = 5


class ColumnExpression(Serializable):
    def __init__(self, **kwargs):
        Serializable.__init__(self, **kwargs)


class FunctionType(Enum):
    NONE = 0
    Macros = 1
    Aggregation = 2
    DatePart = 3
    Length = 4


class SelectQueryColumn(Serializable):
    def __init__(self, caption="", orderDirection=OrderDirection.NONE, orderPosition=-1, isVisible=True,
                 expression=None, **kwargs):
        Serializable.__init__(self, **kwargs)
        self.caption = caption
        self.orderDirection = orderDirection
        self.orderPosition = orderPosition
        self.isVisible = isVisible
        self.Expression = expression


def to_update(obj, original_id):
    obj.operationType = OperationTypes.UPDATE.value
    obj.__dict__['columnValues'].__dict__['items'].__dict__.pop('Id', None)
    obj.__dict__['isForceUpdate'] = True
    obj.filters = IdFilter(original_id)
    return obj


def get_column_expr_from_name(col_names, order_cols):
    o = Serializable()
    for c in col_names:
        orderDirection = OrderDirection.NONE
        orderPosition = -1
        if c == 'CreatedOn':
            orderPosition = 1
            orderDirection = OrderDirection.DESC
        o[c] = SelectQueryColumn(
            orderDirection=orderDirection,
            orderPosition=orderPosition,
            expression=ColumnExpression(
                ExpressionType=SelectQueryExpressionType.SchemaColumn,
                ColumnPath=c
            ))
    return o


# TODO Filters
def count_entities(entity_schema_name):
    obj = Serializable(
        RootSchemaName=entity_schema_name,
        OperationType=OperationTypes.SELECT.value,
        AllColumns=False,
        Columns=Serializable(
            Items=Serializable(
                IdCount=SelectQueryColumn(expression=ColumnExpression(
                    ExpressionType=SelectQueryExpressionType.Function,
                    FunctionType=FunctionType.Aggregation,
                    AggregationType=AggregationType.COUNT,
                    functionArgument=ColumnExpression(
                        ExpressionType=ExpressionTypes.SchemaColumn.value,
                        ColumnPath='Id'
                    )
                ))
            )
        )
    ).toJSON()
    return obj


def select_entities(entity_schema_name, columns=[], order_cols=[], row_count=-1, page_num=0):
    all_columns = False
    if not columns:
        all_columns = True
    is_pageable = True
    if row_count == -1:
        is_pageable = False
    obj = Serializable(
        RootSchemaName=entity_schema_name,
        OperationType=OperationTypes.SELECT.value,
        AllColumns=all_columns,
        Columns=Serializable(
            Items=get_column_expr_from_name(columns, order_cols=order_cols)
        ),
        RowCount=row_count,
        rowsOffset=row_count * page_num,
        IsPageable=is_pageable
    )
    return obj.toJSON()


def select_img_names():
    return {
        "RootSchemaName": 'ListingGalleryImage',
        "OperationType": OperationTypes.SELECT.value,
        "AllColumns": False,
        "Columns": {
            "Items": {
                "Name": {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                         "expression": {"expressionType": 0, "columnPath": "Name"}
                         },
                "Listing": {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                            "expression": {"expressionType": 0, "columnPath": "Listing"}
                            }
            }
        }
    }


def select_by_column(entity, filters, columns=['Id']):
    obj = {
        "RootSchemaName": entity,
        "OperationType": OperationTypes.SELECT.value,
        "AllColumns": False,
        "Columns": {
            "Items": {}
        },
        "filters": {
            "Items": {
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }
    for col in columns:
        obj['Columns']['Items'][col] = {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                                        "expression": {"expressionType": 0, "columnPath": col}
                                        }
    for k, v in filters.items():
        obj['filters']['Items'][k] = {
            "filterType": 1,
            "comparisonType": 3,
            "isEnabled": True,
            "trimDateTimeParameterToDate": False,
            "leftExpression": {"expressionType": 0, "columnPath": k},
            "rightExpression": {
                "expressionType": expression_types['Parameter'],
                "parameter": {
                    "dataValueType": 0,
                    "value": v
                }
            }
        }
    return json.dumps(obj)


def delete_by_column(entity, col_name, col_value):
    return {
        "RootSchemaName": entity,
        "OperationType": OperationTypes.DELETE.value,
        "filters": {
            "items": {
                "primaryColumnFilter": {
                    "filterType": 1,
                    "comparisonType": 3,
                    "isEnabled": True,
                    "trimDateTimeParameterToDate": False,
                    "leftExpression": {"expressionType": 0, "columnPath": col_name},
                    "rightExpression": {
                        "expressionType": expression_types['Parameter'],
                        "parameter": {
                            "dataValueType": 0,
                            "value": col_value
                        }
                    }
                }
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }


class AuthHolder:
    def __init__(self, session, token):
        self.session = session
        self.token = token
