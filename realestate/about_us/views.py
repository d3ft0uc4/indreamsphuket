# coding=utf-8
import smtplib
from email.mime.text import MIMEText
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, FormView
import realestate.settings as app_settings
from realestate.home.views import BaseTemplateView
from realestate.services import *


class VakansiiView(BaseTemplateView, FormView):
    template_name = 'vakansii.html'


class TestimonialsView(BaseTemplateView, FormView):
    template_name = 'testimonials.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['testimonials'] = KnowledgeBase().coll.find({"Type.displayValue": "Сайт: Отзывы"})
        return context


class FAQView(BaseTemplateView, FormView):
    template_name = 'faq.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        with open('static/faq.json') as f:
            context['FAQ'] = json.load(f)
        print context['FAQ']
        return context


class PartnersView(BaseTemplateView, FormView):
    template_name = 'partners.html'

    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        context['partners'] = [
            {'logo': 'http://www.indreamsphuket.ru/uploads/laguna.png',
             'link': 'http://www.lagunaphuket.com/property/?lang=EN'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/erawana-logo.gif',
             'link': 'http://www.erawana.com/'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/img-two-villas-logo.png',
             'link': 'http://www.twovillasholiday.com/villa_and_location_oxygen_style_bangtao.html'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/sansiri.png',
             'link': 'http://www.sansiri.com/ru/international-buyer'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/coco.jpg', 'link': 'http://www.cocovilla.net/'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/the_view.png', 'link': 'http://theviewphuket.com/'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/andara.jpg', 'link': '#'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/pearl-of-naithon-logo.png',
             'link': 'http://www.pearlofnaithon.com/'},
            {'logo': 'http://www.indreamsphuket.ru/uploads/anchan.png', 'link': '#'}
        ]
        return context
