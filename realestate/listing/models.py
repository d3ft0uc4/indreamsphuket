# -*- coding: utf-8 -*-
from decimal import Decimal
import os
import re
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify
import time
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField
from django.contrib.staticfiles.templatetags.staticfiles import static
import random
from unidecode import unidecode
from realestate.mongo_adapter import MongoModel


class AgentManager(models.Manager):
    def active(self, **kwargs):
        return self.filter(active=True, **kwargs)

    def with_listings(self, **kwargs):
        return self.active(listing__isnull=False, **kwargs)


class Agent(models.Model):
    first_name = models.CharField(max_length=30, verbose_name=_('First name'))
    last_name = models.CharField(max_length=30, verbose_name=_('Last name'))
    phone = models.CharField(max_length=15, verbose_name=_('Phone'), null=True, blank=True)
    mobile = models.CharField(max_length=15, verbose_name=_('Cellphone'), null=True, blank=True)
    address = models.CharField(max_length=200, verbose_name=_('Address'), null=True, blank=True)
    image = ImageField(upload_to='agents/', default='', verbose_name=_('Picture'), null=True, blank=True)
    user = models.OneToOneField(User, verbose_name=_('User'), null=True, blank=True)
    active = models.BooleanField(default=False, verbose_name=_('Active'))

    objects = AgentManager()

    @property
    def name(self):
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def email(self):
        return self.user.email if self.user is not None else None

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Agent')
        verbose_name_plural = _('Agents')


class ListingManager(models.Manager):
    pass


class Listing(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'listing')

    objects = ListingManager()

    @property
    def main_image(self):
        return {'absolute_url': static('img/no-photo.jpg'), 'position': 0}

    @property
    def image_list(self):
        return [{'url': static('img/no-photo.jpg'), 'position': 0}]

    @property
    def amenities_list(self):
        return []

    @property
    def bool_amenities_list(self):
        return []

    def _generate_valid_slug(self, name, bpm_id):
        slug = slugify(unidecode(name))
        while (Listing().coll.find({"slug": slug, "_id": {"$ne": bpm_id}}).count()) > 0:
            slug_parts = slug.split('-')
            if slug_parts[-1].isdigit():
                slug_parts[-1] = '%s' % (int(slug_parts[-1]) + 1)
            else:
                slug_parts.append('2')
            slug = '-'.join(slug_parts)
        self.slug = slug
        return slug

    def is_valid_slug(self):
        if self.slug is None or len(self.slug) < 10:
            return False
        match = re.match('[^\w\s-]', self.slug)
        if not match:
            return False
        return self.slug == slugify(self.slug)

    @property
    def absolute_url(self):
        return self.get_absolute_url()

    def get_absolute_url(self):
        return reverse('listing_details', args=[self.slug])

    def get_features(self):
        attributes = []
        return attributes


class District(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'district')


class PropertyType(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'property_type')


class ListingGalleryImage(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'listing_image')


class KnowledgeBase(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'knowledge_base')


class Rates(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'rates')


class BCRentPeriods(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'rent_periods')


class BCPriceMatrix(MongoModel):
    def __init__(self):
        MongoModel.__init__(self, 'price_matrix')


localizable_strings = {
    'Listing': "Листинг",
    'Id': "Id",
    'CreatedOn': "Дата создания",
    'CreatedBy': "Создал",
    'ModifiedOn': "Дата изменения",
    'ModifiedBy': "Изменил",
    'ProcessListeners': "Активные процессы",
    'Name': "Название",
    'Property': "Объект",
    'Status': "Статус",
    'ListingType': "Тип листинга",
    'Owner': "Ответственный",
    'Price': "Стоимость",
    'Notes': "Заметки",
    'Currency': "Валюта",
    'Photo': "Фото",
    'StartDate': "Дата начала",
    'PropertyCategory': "Категория",
    'PropertyType': "Тип объекта",
    'Description': "Описание",
    'Address': "Адрес",
    'Zip': "Индекс",
    'Latitude': "Широта",
    'Longitude': "Долгота",
    'City': "Город",
    'Region': "Область/Штат",
    'Country': "Страна",
    'Contact': "Контакт",
    'Account': "Контрагент",
    'IntegrationStatus': "Статус интеграции",
    'ExternalSource': "Источник",
    'IntegrationSettings': "Настройки интеграции",
    'ExternalLink': "Ссылка",
    'HouseNumber': "Номер дома",
    'Street': "Улица",
    'PriceChangeReason': "Причина изменения стоимости",
    'District': "Район города",
    'LeadType': "Тип листинга",
    'BCBOOLEAN': "Готовое",
    'BCSTRINGBedroom': "Спален",
    'BCSTRINGGuest': "Гостей",
    'BCINTEGERBathroom': "Ванных",
    'BCSTRINGFloor': "Этажей",
    'BCLOOKUPFurniturs': "Мебель",
    'BCLOOKUPPool': "Бассейн",
    'BCLOOKUPSecurity': "Охрана",
    'BCLOOKUPAdditionally': "Дополнительно",
    'BCLOOKUPOwnershipform': "Форма владения",
    'BCBOOLEANFreeWiFi': "Бесплатный Wi-Fi",
    'BCBOOLEANFreeParking': "Бесплатная парковка",
    'BCLOOKUPBeachFor': "Недалеко от пляжа",
    'BCFLOATBuiltUpArea': "Площадь застройки",
    'BCFLOATLandArea': "Площадь участка",
    'BCSTRINGRent': "Сдача",
    'BCBOOLEANInstallments': "Рассрочка выплат",
    'BCLOOKUPBeachTo': "До пляжа",
    'BCLOOKUPTypetransaction': "Тип сделки",
    'BCSTRINGannualIncome': "Гарантированный годовой доход",
    'BCSTRINGamountBills': "Размер коммунальных платежей",
    'BCBOOLEAN1floor': "1 этаж",
    'BCBOOLEAN2floor': "2 этажа",
    'BCBOOLEAN3floor': "3 этажа",
    'BCBOOLEANLivingRoom': "Гостинная",
    'BCBOOLEANRestroom': "Комната отдыха",
    'BCBOOLEANCanteen': "Столовая",
    'BCBOOLEANKitchen': "Кухня",
    'BCBOOLEANKitchenFor': "Дополнительная кухня для повара",
    'BCBOOLEANBar': "Бар",
    'BCBOOLEANRoomForStaff': "Комната для обслуживающего персонала",
    'BCBOOLEANCabinet': "Кабинет",
    'BCBOOLEANGym': "Тренажерный зал",
    'BCBOOLEANSPAArea': "СПА-зона",
    'BCBOOLEANHomeCinema': "Домашний кинотеатр",
    'BCBOOLEANBilliards': "Бильярд",
    'BCBOOLEANSauna': "Сауна",
    'BCBOOLEAN1bedroom': "1 спальня",
    'BCBOOLEAN2bedroom': "2 спальни",
    'BCBOOLEAN3bedroom': "3 спальни",
    'BCBOOLEAN4bedroom': "4 спальни",
    'BCBOOLEAN5bedroom': "5 спален",
    'BCBOOLEAN6bedroom': "6 спален",
    'BCBOOLEAN7bedroom': "7 спален",
    'BCBOOLEAN8bedroom': "8 спален",
    'BCBOOLEAN1bathroom': "1 ванная комната",
    'BCBOOLEAN2bathroom': "2 ванных комнаты",
    'BCBOOLEAN3bathroom': "3 ванных комнаты",
    'BCBOOLEAN4bathroom': "4 ванных комнаты",
    'BCBOOLEAN5bathroom': "5 ванных комнат",
    'BCBOOLEAN6bathroom': "6 ванных комнат",
    'BCBOOLEAN7bathroom': "7 ванных комнат",
    'BCBOOLEANElevator': "Лифт",
    'BCBOOLEANToiletForGuest': "Гостевой туалет",
    'BCBOOLEANJacuzzi': "Джакузи",
    'BCBOOLEANBathUnderSky': "Душ под открытым небом",
    'BCBOOLEANBalcony': "Балкон",
    'BCBOOLEANPatioOnTheRoof': "Патио на крыше",
    'BCBOOLEANTennis': "Настольный теннис",
    'BCBOOLEANOpenTerrace': "Открытая терраса",
    'BCBOOLEANBedroomOut': "Одна из спален в отдельном павильоне",
    'BCBOOLEANconditioner': "Кондиционер",
    'BCBOOLEANTV': "ТВ",
    'BCBOOLEANFan': "Вентилятор",
    'BCBOOLEANStereo': "Стереосистема",
    'BCBOOLEANViewSee': "Вид на море",
    'BCBOOLEANDVD': "DVD плеер",
    'BCBOOLEANDockStation': "Докстанция",
    'BCBOOLEANWasher': "Стиральная машина",
    'BCBOOLEANDryingMachine': "Сушильная машина",
    'BCBOOLEANIron': "Утюг",
    'BCBOOLEANFridge': "Холодильник",
    'BCBOOLEANWiFi': "Wi-Fi  интернет",
    'BCBOOLEANDishwasher': "Посудомоечная машина",
    'BCBOOLEANWinecabinet': "Винный шкаф",
    'BCBOOLEANcooker': "Плита",
    'BCBOOLEANBake': "Печь",
    'BCBOOLEANMicrowave': "Микроволновая печь",
    'BCBOOLEANKettle': "Чайник",
    'BCBOOLEANCoffeemachine': "Кофемашина",
    'BCBOOLEANCoffeemaker': "Кофеварка",
    'BCBOOLEANToaster': "Тостер",
    'BCBOOLEANBlender': "Блендер",
    'BCBOOLEANDishes': "Посуда",
    'BCBOOLEANSafe': "Сейф",
    'BCBOOLEANBuiltinHomeAppliances': "Встроенная бытовая техника",
    'BCBOOLEANBeach': "Пляж",
    'BCBOOLEANSupermarket': "Супермаркет",
    'BCBOOLEANRestaurants': "Рестораны",
    'BCBOOLEANMassage': "Массаж",
    'BCBOOLEANBars': "Бары",
    'BCBOOLEANSPACentre': "СПА-салоны",
    'BCBOOLEANNightClub': "Ночной клуб",
    'BCBOOLEANSchool': "Школа",
    'BCBOOLEANPlayschool': "Детский сад",
    'BCBOOLEAN3Beach': "3 пляжа",
    'BCBOOLEAN2Beach': "2 пляжа",
    'BCBOOLEANShops': "Магазины",
    'BCBOOLEANMarket': "Рынок",
    'BCBOOLEANBank': "Банк",
    'BCBOOLEANGolf': "Гольф- поле",
    'BCBOOLEANCort': "Теннисный корт",
    'BCBOOLEANHotel5': "Отель 5*",
    'BCBOOLEANHotel': "Отели",
    'BCBOOLEANBeachClub': "Пляжный клуб",
    'BCBOOLEANWaterSport': "Водные виды спорта",
    'BCBOOLEANHospital': "Больница",
    'BCBOOLEANPirs': "Пирс",
    'BCBOOLEANShow': "Шоу",
    'BCBOOLEANAquaPark': "Аквапарк",
    'BCBOOLEANChildrenPark': "Детский развлекательный парк",
    'BCBOOLEANAirport': "Аэропорт",
    'BCBOOLEANCleaning': "Уборка(ДО)",
    'BCBOOLEANChangeLinen': "Смена белья(ДО)",
    'BCBOOLEANUnlimitedInternet': "Безлимитный интернет(ДО)",
    'BCBOOLEANCleaningGarden': "Уборка сада(ДО)",
    'BCBOOLEANCleaningPool': "Чистка бассейна(ДО)",
    'BCLOOKUPElectricity': "Электричество(ДО)",
    'BCLOOKUPWater': "Вода(ДО)",
    'BCBOOLEANTransferFromAirport': "Трансфер из аэропорта(ДО)",
    'BCBOOLEANTransferFromAndToTheAirport': "Трансфер из аэропорта и обратно(ДО)",
    'BCBOOLEANMaintenanceOfCommonArea': "Обслуживание общей территории(ДО)",
    'BCBOOLEANDriver': "Водитель(ДО)",
    'BCLOOKUPCook': "Повар(ДО)",
    'BCBOOLEANConcierge': "Консьерж(ДО)",
    'BCBOOLEANShuttlebass': "Шатл-бас(ДО)",
    'BCBOOLEANBreakfasts': "Завтраки(ДО)",
    'BCBOOLEANTV1': "TV(ДО)",
    'BCLOOKUPTaxes': "Таксы и налоги(ДО)",
    'BCBOOLEANextraBed': "Дополнительная кровать",
    'BCBOOLEANCheckInFrom9': "Заселение с 9 утра до 6 вечера 100 USD",
    'BCBOOLEANCheckInFrom6': "Заселение с 6 вечера до 9 утра 150 ",
    'BCLOOKUPMassage': "Массаж(ДО)",
    'BCLOOKUPCleaning': "Уборка",
    'BCLOOKUPChangeLinen': "Смена белья",
    'BCBOOLEANInternet': "Безлимитный интернет",
    'BCBOOLEANCleaningGardenFree': "Уборка сада",
    'BCBOOLEANCleaningPoolFree': "Чистка бассейна",
    'BCBOOLEANElectricity': "Электричество",
    'BCBOOLEANWater': "Вода",
    'BCBOOLEANTransferFromAirportFree': "Трансфер из аэропорта",
    'BCBOOLEANTransferFromAirportTo': "Трансфер из аэропорта и обратно",
    'BCBOOLEANMaintenanceCommonArea': "Обслуживание общей территории",
    'BCBOOLEANDriverFree': "Водитель",
    'BCBOOLEANCookFree': "Повар",
    'BCBOOLEANConciergeFree': "Консьерж",
    'BCBOOLEANBusFree': "Шатл-бас",
    'BCBOOLEANTVFree': "TV",
    'BCLOOKUPBrekfastFree': "Завтраки",
    'BCBOOLEANTVRusFree': "Российское TV",
    'BCBOOLEANKartinaTVFree': "Kartina TV(150 российских каналов)",
    'BCBOOLEANTaxsFree': "Таксы и налоги",
    'BCBOOLEANWelcomePackFree': "Welcome- pack",
    'BCBOOLEANRentAvto': "Аренда автомобиля",
    'BCBOOLEANRentYacht': "Аренда яхты",
    'BCBOOLEANRentHelicopter': "Аренда вертолета",
    'BCBOOLEANRentForBaby': "Аренда детского инвентаря",
    'BCBOOLEANStaffRent': "Обслуживающий персонал",
    'BCBOOLEANIndividualExcursions': "Индивидуальные экскурсии",
    'BCBOOLEANShowEntertainment': "Шоу и развлечения",
    'BCBOOLEANShopping': "Шоппинг",
    'BCBOOLEANOrderingTables': "Заказ столов в лучших ресторанах",
    'BCBOOLEANMassageSPA': "Массаж в лучших спа-салонах",
    'BCBOOLEANholidaysEvents': "Организация праздников и мероприятий",
    'BCBOOLEANViewOnMontain': "Вид на горы",
    'BCBOOLEANViewOnLake': "Вид на озеро",
    'BCBOOLEANParkingOut': "Парковка",
    'BCBOOLEANParkingIn': "Крытая парковка",
    'BCBOOLEANgoal': "Автоматические ворота",
    'BCBOOLEANMyselfPool': "Частный бассейн",
    'BCBOOLEANChildrensPool': "Детский бассейн",
    'BCBOOLEANCommonPool': "Общий бассейн",
    'BCBOOLEANJacizziOut': "Джакузи на территории",
    'BCBOOLEANTropicalGarden': "Тропический сад",
    'BCBOOLEANBBQArea': "Барбекю-зона",
    'BCBOOLEANCommonBBQArea': "Общая барбекю-зона",
    'BCBOOLEANCoveredInGarden': "Крытая беседка в саду",
    'BCBOOLEANDiningRoomOpenAir': "Столовая на открытом воздухе",
    'BCBOOLEANSunbedsOut': "Шезлонги",
    'BCBOOLEANReception': "Ресепшн",
    'BCBOOLEANSecuritiOut': "Охрана территории",
    'BCBOOLEANSafetySystem': "Система безопасности",
    'BCBOOLEANChildrensClub': "Детский клуб",
    'BCBOOLEANARestaurant': "Ресторан",
    'BCBOOLEANABar': "Бар",
    'BCBOOLEANSPACentreOut': "СПА-центр",
    'BCBOOLEANSaunaCommonOut': "Общая сауна",
    'BCBOOLEANGymOutCommon': "Общий тренажерный зал",
    'BCBOOLEANCommonPlaygroundOut': "Общая детская площадка",
    'BCBOOLEANBusServiceOut': "Шатл-бас сервис",
    'BCBOOLEANTenniseCortOut': "Теннисный корт на территории",
    'BCBOOLEANCoveredroof': "Крытая беседка на крыше",
    'BCLOOKUPOfferType': "Тип предложений",
    'BCSiteID': "ID Сайта",
    'BCBOOLEANEntryRoom': "Гардеробная комната",
    'BCBOOLEANBarOut': "Бар на территории",
    'BCBOOLEANPrianXML': "Выгрузка на prian.ru",
    'BCBOOLEANPartnersXML': "Выгрузка партнёрам",
    'BCBOOLEANHomesOverseasXML': "Выгрузка на HomesOverseas",
    'BCSTRINGGallery': "Ссылка на галерею",
    'BCSiteContact': "Контакт с сайта",
    'BCSTRINGPaymentTerm1': "Условие оплаты 1",
    'BCSTRINGPaymentTerm2': "Условие оплаты 2",
    'BCSTRINGPaymentTerm3': "Условие оплаты 3",
    'BCSTRINGPaymentTerm4': "Условие оплаты 4",
    'BCSTRINGPaymentTerm5': "Условие оплаты 5",
    'BCBOOLEANRassr': "Рассрочка на период строительства",
    'BCSTRINGRassrPeriod': "Рассрочка на период до",
    'BCBOOLEANFullPayment': "Полная оплата",
    'BCBOOLEANDogovornoy': "Договорной",
    'BCSTRINGObjectID': "ID объекта",
    'BCSTRINGMebPackage': "Мебельный пакет",
    'BCSTRINGDealRegistrationFee': "Расходы на регистрацию сделки",
    'BCSTRINGAmorFee': "Амортизационный взнос",
    'BCSTRINGRegistrationFee': "Регистрационный сбор",
    'BCSTRINGGerbFee': "Гербовый сбор",
    'BCSTRINGFreeholdFee': "Доплата за фрихолд",
    'BCSTRINGPropFee': "Коммунальные платежи",
    'BCSTRINGFee1': "Установка счетчиков электричества и воды",
    'BCSTRINGSpecialBusinessFee': "Специальный бизнес-налог",
    'BCSTRINGOtherFee': "Удерживаемый налог",
    'BCSTRINGCalendar': "Ссылка на календарь",
    'BCSTRINGMapLink': "Ссылка на Яндекс карты",
    'BCPricePerDay': "Цена за день",
    'BCBOOLEANProp89': "Встроенная мебель (вкл)",
    'BCBOOLEANProp90': "Мебель (вкл)",
    'BCBooleanProp91': "Кухня (вкл)",
    'BCBOOLEANProp92': "Встроенная бытовая техника (вкл)",
    'BCBOOLEANProp93': "Бытовая техника (вкл)",
    'BCBOOLEANProp94': "Предметы интерьера (вкл)",
    'BCBOOLEANProp95': "Ландшафтный дизайн (вкл)",
    'BCBOOLEANProp96': "Оформление сделки (вкл)",
    'MlsNumber': "Mls",
    'GeoAddressId': "Id адреса",
    'GeoServiceResponse': "Ответ адресной службы",
    'AddressLink': "Ссылка на адрес",
    'GeoService': "Адресная интернет-служба",
    'LatitudeEx': "Широта (служебная)",
    'LongitudeEx': "Долгота (служебная)",
    'CityText': "Город (служебное)",
    'CityDescriptionText': "Город (описание, служебное поле)",
    'RegionText': "Область/регион (служебное поле)",
    'RegionDescriptionText': "Область/регион, описание (служебное)",
    'CountryText': "Страна (служебное)",
    'DistrictText': "Район (служебное)"
}

main_props = [
    'Бассейн',
    'До пляжа',
    'Бесплатная парковка',
    'Дополнительно',
    'Бесплатный Wi-Fi',
    'Контакт',
    'Ванных',
    'Мебель',
    'Гарантированный годовой доход',
    'Недалеко от пляжа',
    'Гостей',
    'Охрана',
    'Готовое',
    'Площадь застройки',
    'Тип предложений',
    'Площадь участка',
    'Тип сделки',
    'Размер коммунальных платежей',
    'Форма владения',
    'Рассрочка выплат',
    'Этажей',
    'Сдача',
    'Спален']

mp_dict = {
    'Бассейн': 'swimming-pool-with-stairs.png',
    'До пляжа': 'house-location.png',
    'Бесплатная парковка': 'parking-facilities.png',
    'Дополнительно': 'building-land.png',
    'Бесплатный Wi-Fi': 'wifi-signal.png',
    'Ванных': 'bathtub-and-shower-head.png',
    'Мебель': 'individual-sofa.png',
    'Гарантированный годовой доход': 'apartment-prices.png',
    'Недалеко от пляжа': 'home-map-location.png',
    'Гостей': 'house-of-your-dreams.png',
    'Охрана': 'secure-house.png',
    'Готовое': 'building-a-house.png',
    'Площадь застройки': 'properties-info.png',
    'Тип предложений': 'house-sale-agreement.png',
    'Площадь участка': 'plan-of-a-house.png',
    'Тип сделки': 'home-dispute.png',
    'Размер коммунальных платежей': 'house-sale-prices.png',
    'Форма владения': 'house-key.png',
    'Рассрочка выплат': 'house-prize-discount.png',
    'Этажей': 'houses-renovation.png',
    'Сдача': 'signing-a-contract.png',
    'Спален': 'long-sofa.png'

}

blag_doma = ['1 ванная комната', '1 спальня', '1 этаж', '2 ванных комнаты', '2 спальни', '2 этажа', '3 ванных комнаты',
             '3 спальни', '3 этажа', '4 ванных комнаты', '4 спальни', '5 ванных комнат', '5 спален', '6 ванных комнат',
             '6 спален', '7 ванных комнат', '7 спален', '8 спален', 'Балкон', 'Бар', 'Бильярд', 'Гардеробная комната',
             'Гостевой туалет', 'Гостинная', 'Джакузи', 'Домашний кинотеатр', 'Дополнительная кухня для повара',
             'Душ под открытым небом', 'Кабинет', 'Комната для обслуживающего персонала', 'Комната отдыха', 'Кухня',
             'Лифт', 'Настольный теннис', 'Одна из спален в отдельном павильоне', 'Открытая терраса', 'Патио на крыше',
             'СПА-зона', 'Сауна', 'Столовая', 'Тренажерный зал']

blag_territorii = ['Автоматические ворота',
                   'Бар на территории',
                   'Барбекю-зона',
                   'Вид на горы',
                   'Вид на море',
                   'Вид на озеро',
                   'Детский бассейн',
                   'Детский клуб',
                   'Джакузи на территории',
                   'Крытая беседка в саду',
                   'Крытая беседка на крыше',
                   'Крытая парковка',
                   'Общая барбекю-зона',
                   'Общая детская площадка',
                   'Общая сауна',
                   'Общий бассейн',
                   'Общий тренажерный зал',
                   'Охрана территории',
                   'Парковка',
                   'Ресепшн',
                   'Ресторан',
                   'СПА-центр',
                   'Система безопасности',
                   'Столовая на открытом воздухе',
                   'Теннисный корт на территории',
                   'Тропический сад',
                   'Частный бассейн',
                   'Шатл-бас сервис',
                   'Шезлонги']
technika = ['DVD плеер'
            'Wi-Fi интернет',
            'Блендер',
            'Вентилятор',
            'Винный шкаф',
            'Встроенная бытовая техника',
            'Докстанция',
            'Кондиционер',
            'Кофеварка',
            'Кофемашина',
            'Микроволновая печь',
            'Печь',
            'Плита',
            'Посуда',
            'Посудомоечная машина',
            'Сейф',
            'Стереосистема',
            'Стиральная машина',
            'Сушильная машина',
            'ТВ',
            'Тостер',
            'Утюг',
            'Холодильник',
            'Чайник']

infrastructure_3km = ['2 пляжа',
                      'Аквапарк',
                      '3 пляжа',
                      'Аэропорт',
                      'Банк',
                      'Бары',
                      'Больница',
                      'Водные виды спорта',
                      'Гольф- поле',
                      'Детский развлекательный парк',
                      'Детский сад',
                      'Магазины',
                      'Массаж',
                      'Ночной клуб',
                      'Отели',
                      'Отель 5*',
                      'Пирс',
                      'Пляж',
                      'Пляжный клуб',
                      'Рестораны',
                      'Рынок',
                      'СПА-салоны',
                      'Супермаркет',
                      'Теннисный корт',
                      'Школа',
                      'Шоу']

oplata_otdelno = ['Мебельный пакет',
                  'Расходы на регистрацию сделки',
                  'Амортизационный взнос',
                  'Регистрационный сбор',
                  'Гербовый сбор',
                  'Доплата за фрихолд',
                  'Коммунальные платежи',
                  'Установка счетчиков электричества и воды',
                  'Специальный бизнес-налог',
                  'Удерживаемый налог']

dop_oplata = ['TV(ДО)',
              'Безлимитный интернет(ДО)',
              'Вода(ДО)',
              'Водитель(ДО)',
              'Дополнительная кровать',
              'Завтраки(ДО)',
              'Заселение с 6 вечера до 9 утра 150',
              'Заселение с 9 утра до 6 вечера 100 USD',
              'Консьерж(ДО)',
              'Массаж(ДО)',
              'Обслуживание общей территории(ДО)',
              'Повар(ДО)',
              'Смена белья(ДО)',
              'Таксы и налоги(ДО)',
              'Трансфер из аэропорта(ДО)',
              'Трансфер из аэропорта и обратно(ДО)',
              'Уборка(ДО)',
              'Уборка сада(ДО)',
              'Чистка бассейна(ДО)',
              'Шатл-бас(ДО)',
              'Электричество(ДО)']

v_stoimost_vkl = ['Kartina TV(150 российских каналов)',
                  'TV',
                  'Welcome- pack',
                  'Безлимитный интернет',
                  'Вода',
                  'Водитель',
                  'Завтраки',
                  'Консьерж',
                  'Повар',
                  'Обслуживание общей территории',
                  'Российское TV',
                  'Смена белья',
                  'Таксы и налоги',
                  'Трансфер из аэропорта',
                  'Трансфер из аэропорта и обратно',
                  'Уборка',
                  'Уборка сада',
                  'Чистка бассейна',
                  'Шатл-бас',
                  'Электричество',
                  'Встроенная мебель (вкл)',
                  'Мебель (вкл)',
                  'Кухня (вкл)',
                  'Встроенная бытовая техника (вкл)',
                  'Бытовая техника (вкл)',
                  'Предметы интерьера (вкл)',
                  'Ландшафтный дизайн (вкл)',
                  'Оформление сделки (вкл)']

uslugi_konsierzha = ['Аренда автомобиля',
                     'Аренда вертолета',
                     'Аренда детского инвентаря',
                     'Аренда яхты',
                     'Заказ столов в лучших ресторанах',
                     'Индивидуальные экскурсии',
                     'Массаж в лучших спа-салонах',
                     'Обслуживающий персонал',
                     'Организация праздников и мероприятий',
                     'Шоппинг',
                     'Шоу и развлечения']
