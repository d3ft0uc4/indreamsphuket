from django.core.management.base import BaseCommand, CommandError

from realestate.services import update_rates


class Command(BaseCommand):
    def handle(self, *args, **options):
        update_rates()
