from django.core.management.base import BaseCommand

from realestate.listing.cian.cian import create_xml
from realestate.services import sync


class Command(BaseCommand):
    def handle(self, *args, **options):
        create_xml()
