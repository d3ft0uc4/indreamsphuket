from django.core.management.base import BaseCommand, CommandError
from realestate.services import get_ical


class Command(BaseCommand):
    def handle(self, *args, **options):
        print options
        if not len(args):
            raise Exception('Specify url')
        get_ical(args[0])

    def add_arguments(self, parser):
        parser.add_argument(
            '-u',
            '--url',
            action='store_true',
            default=False,
            help='URL'
        )
