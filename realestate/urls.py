from django.conf.urls import *
from django.conf import settings
# from django.contrib import admin
from realestate.listing import views as listing_views
from realestate.home.views import *
from realestate.about_us.views import *
from django.conf.urls import handler400, handler403, handler404, handler500
from django.views.generic import TemplateView

# admin.autodiscover()

handler404 = handle404
# handler500 = 'my_app.views.server_error'


urlpatterns = patterns(
    '',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^properties/$', PropertiesView.as_view(), name='properties'),
    url(r'^rent/$', RentView.as_view(), name='rent'),
    url(r'^buy/$', BuyView.as_view(), name='buy'),
    url(r'^advanced-search/', AdvancedSearchView.as_view(), name='advanced_search'),
    # url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^blog/$', BlogView.as_view(), name='blog'),
    url(r'^kontakty/$', ContactView.as_view(), name='contact'),
    url(r'^handle_forms/$', handle_forms, name='handle_forms'),
    url(r'^calc/$', calc, name='calc'),
    url(r'^ajax/$', handle_ajax, name='ajax'),
    url(r'^single_property/$', SinglePropertyView.as_view(), name='single_property'),
    url(r'^property/(?P<slug>[\w-]+)/$', SinglePropertyView.as_view(), name='property'),
    url(r'^service/$', ServiceView.as_view(), name='service'),
    # TODO
    url(r'^testimonials/$', TestimonialsView.as_view(), name='testimonials'),
    url(r'^vakansii/$', VakansiiView.as_view(), name='vakansii'),
    url(r'^dostizheniya/$', ServiceView.as_view(), name='dostizheniya'),
    url(r'^nashi-partnery/$', PartnersView.as_view(), name='nashi-partnery'),
    url(r'^faq/$', FAQView.as_view(), name='faq'),
    url(r'^fotogalereya/$', PhotosView.as_view(), name='fotogalereya'),
    url(r'^news/$', NewsView.as_view(), name='news'),
    url(r'^news/(?P<slug>[\w-]+)/$', SingleNewsView.as_view(), name='single_news'),
    url(r'^stati/$', StatiView.as_view(), name='stati'),
    url(r'^stati/(?P<slug>[\w-]+)/$', SingleStatiView.as_view(), name='single_stati'),

    # TODO
    url(r'^plyazhy-phuketa/$', BaseTemplateView.as_view(template_name='plyazhy-phuketa.html'), name='plyazhy-phuketa'),
    url(r'^villa-ili-otel/$', VillaHotelView.as_view(), name='villa-ili-otel'),
    url(r'^edem-na-phuket-s-detmi/$', BaseTemplateView.as_view(template_name='edem-na-phuket-s-detmi.html'),
        name='edem-na-phuket-s-detmi'),
    url(r'^investoram/$', InvestoramView.as_view(), name='investoram'),
    url(r'^vizy-v-tailande/$', BaseTemplateView.as_view(template_name='vizy-v-tailande.html'), name='vizy-v-tailande'),
    url(r'^laguna-outrigger-village-na-phukete/$', LagunaView.as_view(), name='laguna-outrigger-village-na-phukete'),
    url(r'^excursions/$', ExcursionsView.as_view(), name='excursions'),
    url(r'^excursions/(?P<slug>[\w-]+)/$', single_excursion, name='excursions_single'),
    # TODO
    url(r'^usloviya-bronirovaniya/$', BaseTemplateView.as_view(template_name='usloviya-bronirovaniya.html'),
        name='usloviya-bronirovaniya'),
    url(r'^pravila-prozhivaniya/$', BaseTemplateView.as_view(template_name='pravila-prozhivaniya.html'),
        name='pravila-prozhivaniya'),
    url(r'^investoram/$', ServiceView.as_view(), name='investoram'),
    url(r'uslugi-dlya-vladelcev-nedvizhimosti/$', ServiceView2.as_view(), name='uslugi-dlya-vladelcev-nedvizhimosti'),
    url(r'currency/$', set_currency, name='set_currency'),
    url(r'language/$', set_lang, name='set_lang'),
    url(r'add-new-property/$', AddNewPropertyView.as_view(), name='add-new-property'),
    url(r'getfiles/$', getfiles, name='getfiles'),
    url(r'sync/$', sync, name='sync'),
    # Static Pages
    (r'^i18n/', include('django.conf.urls.i18n'))
)
if settings.DEBUG:
    urlpatterns += patterns(
        '',
        (r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
