import pymongo

conn = pymongo.MongoClient()
db = conn['indreamsphuket']


class MongoModel:
    def __init__(self, coll):
        self._coll = db[coll]

    @property
    def coll(self):
        return self._coll
